<?php

  /* A message for people to see on the main page */
  $cfg_MOTD = '';

  $cfg_refresh_time = 60 * 1; // Payout time in seconds.
  $cfg_real_refresh_time = $cfg_refresh_time; // Refresh time of the claim page.

  /* Enable and disable currencies. */
  $cfg_BCH_enabled  = false;
  $cfg_BTC_enabled  = false;
  $cfg_XRP_enabled  = false;
  $cfg_DASH_enabled = false;
  $cfg_DGB_enabled  = false;
  $cfg_DOGE_enabled = false;
  $cfg_ETH_enabled  = false;
  $cfg_WAVES_enabled = false;
  $cfg_STRAT_enabled = false; // New
  $cfg_LTC_enabled  = false;
  $cfg_POT_enabled  = false;
  $cfg_PPC_enabled  = false;
  $cfg_TRX_enabled  = false;
  $cfg_XMR_enabled  = false;
  $cfg_LSK_enabled  = false;
  $cfg_BCN_enabled  = false;
  $cfg_ZEC_enabled  = false;
  $cfg_NEO_enabled  = false;
  $cfg_EXG_enabled  = false;
  $cfg_EXS_enabled  = false;
  $cfg_ETC_enabled  = false; // New

  $cfg_BCH_amount  = intval((20 / 60) * $cfg_refresh_time);
  $cfg_BTC_amount  = intval((2 / 60) * $cfg_refresh_time);
  $cfg_XRP_amount  = intval((2000 / 60) * $cfg_refresh_time);
  $cfg_DASH_amount = intval((20 / 60) * $cfg_refresh_time);
  $cfg_DGB_amount = intval((2000 / 60) * $cfg_refresh_time);
  $cfg_DOGE_amount = intval((10000 / 60) * $cfg_refresh_time);
  $cfg_ETH_amount  = intval((20 / 60) * $cfg_refresh_time);
  $cfg_WAVES_amount = intval((100 / 60) * $cfg_refresh_time);
  $cfg_STRAT_amount = intval((100 / 60) * $cfg_refresh_time);
  $cfg_LTC_amount  = intval((20 / 60) * $cfg_refresh_time);
  $cfg_POT_amount  = intval((200 / 60) * $cfg_refresh_time);
  $cfg_PPC_amount  = intval((25 / 60) * $cfg_refresh_time);
  $cfg_TRX_amount  = intval((25 / 60) * $cfg_refresh_time);
  $cfg_XMR_amount = intval((20 / 60) * $cfg_refresh_time);
  $cfg_LSK_amount  = intval((200 / 60) * $cfg_refresh_time);
  $cfg_BCN_amount  = intval((20000 / 60) * $cfg_refresh_time);
  $cfg_ZEC_amount  = intval((5 / 60) * $cfg_refresh_time);
  $cfg_NEO_amount  = intval((5 / 60) * $cfg_refresh_time);
  $cfg_EXG_amount  = intval((5 / 60) * $cfg_refresh_time);
  $cfg_EXS_amount  = intval((500 / 60) * $cfg_refresh_time);
  $cfg_ETC_amount  = intval((5 / 60) * $cfg_refresh_time);

  /* Make sure that the faucet is set up under the "PTP" and "Mining" categories on Faucet Hub's
   * faucet manager page, or users could get their accounts frozen for claiming too often! */

  /* ExpressCrypto API Key(s)
   * You can set them all to the same key if you want.
   * Some people just like to register a different "faucet" for each currency. */
  $cfg_BCH_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_BLK_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_BTC_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_XRP_api_key = 'yourexpresscryptoapicode12345678910';
  $cfg_DASH_api_key = 'yourexpresscryptoapicode12345678910';
  $cfg_DGB_api_key = 'yourexpresscryptoapicode12345678910';
  $cfg_DOGE_api_key = 'yourexpresscryptoapicode12345678910';
  $cfg_ETH_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_WAVES_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_STRAT_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_LTC_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_POT_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_PPC_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_TRX_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_XMR_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_LSK_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_BCN_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_ZEC_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_NEO_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_EXG_api_key  = 'yourexpresscryptoapicode12345678910';
  $cfg_EXS_api_key  = 'yourexpresscryptoapicode12345678910';

  /* Set this to true and the faucet will automatically ban some botters and abusers by adding `deny from IP_ADDRESS` lines to /.htaccess */
  /* Leave this disabled unless your server uses .htaccess files and you are fine with an automated script modifying it! */
  $cfg_enable_ban = false;

  /* Should return the user's IP address. Change it if you use CloudFlare. */
  function get_user_ip() {
    return $_SERVER['REMOTE_ADDR']; // Without CloudFare

    //return $_SERVER["HTTP_CF_CONNECTING_IP"] // With CloudFare
  }

  $cfg_cookie_key = 'DIE BOTS DIE'; // Set this to a secret string that only you know.

  /* The default CAPTCHA is coinhive, and the default shortlink is eliwin;
   * you can change them, but you've got to rewrite captcha.lib.php or shortlink.lib.php yourself. */

  $cfg_use_captcha = false; // Set this to false to disable the CAPTCHA
  if($cfg_use_captcha){
    $cfg_captcha_type = 1; // 1. Solvemedia  2. raincaptcha   3. recaptcha   4. Hcaptcha

       // Edit your captcha keys here
       $cfg_solvemedia_public = 'vzi3V635LMmW0.GL73bX7u7-zHjgzd65';
      $cfg_solvemedia_private = 'S981uwZ9zry7gMvTngAoDiAzsLie2YbV';
      $cfg_solvemedia_hash = 'yIOPND1v8Y0Q8kumRurBnTXuRItzP7qK';
      $cfg_raincaptcha_public = '7392bff3dc89793e9aeec8f90bb59b45b8bd445c';
      $cfg_raincaptcha_secret = 'Ynmg_pVpwHDwEtzEIOKHIWAU_QKWaldw';
      $cfg_recaptcha_site_key = '6LeKF3sUAAAAAJZglWVogSBKOHeqH78eOHXLw79K';
      $cfg_recaptcha_secret_key = '6LeKF3sUAAAAAMTH1y5JmpPsGKXYGxubJZHeh7_s';
      $cfg_h_captcha_site_key = '277d934d-3bdc-49bd-90bb-a73f9e0eef0d';
      $cfg_h_captcha_secret_key = '0x30F60cAD9CC7225A6C5c27e08c0c4fd6E84724E4';
    }

  $cfg_use_shortlink = true; // go to shortlink.php to configure your shortlinks 
 
  $cfg_enable_blackbox = true; // Whether to check proxies with blackbox on the claim page.

  $cfg_enable_nastyhosts = false; // Whether to check proxies with nastyhosts on the claim page.
  if ($cfg_enable_nastyhosts) {
    $cfg_nastyhost_whitelist = [ // IP addresses that you don't want to check
      'IP address' => 'description (can be anything you want)',
      '8.8.8.8' => 'Generic IP address',
      '127.0.0.1' => 'someone',
    ];
  }

  $cfg_enable_iphub = false; // If you want to use IPHub instead (might as well disable NastyHosts first) (you _can_ use both, if you hate everyone)
  if ($cfg_enable_iphub) {
    $cfg_iphub_key = 'XXXREDACTEDXXXXXXXXXXXXXXXXXXXXXSnB4TDd0c1hTbXpI';
    $cfg_iphub_block_level = 1; // https://iphub.info/api

    $cfg_iphub_whitelist = [ // IP addresses that you don't want to check
      'IP address' => 'description (can be anything you want)',
      '8.8.8.8' => 'Generic IP address',
      '127.0.0.1' => 'someone',
    ];
  }

  /* Google Analytics options. */
  $cfg_enable_google_analytics = FALSE;
  if ($cfg_enable_google_analytics) {
    $cfg_ga_ID = ''; // your tracking ID
    /* Be sure to go to
     *  [Admin > All Web Site Data > View Settings]
     * and set "Exclude URL Query Parameters" to:
     *   r,rc,address,currency,key
    */
  }

  $user_token = ""; // Your User Token (that you can edit on expresscrypto)

  $cfg_ec_username = 'your_express_username'; // Your ExpressCrypto username.
  $cfg_site_name = 'DutchyCorp Express FloodGate 0.1'; // The faucet name.
  $cfg_site_url = 'your_url'; // The URL of the faucet. ex https://dutchybig.ovh
  $cfg_REF_PERCENTAGE = 20;
  /* Set this to the version of the faucet source you are using. (see http://semver.org)
   * If you change the source, be sure to add "+mod" (modified) to the version! */
  $cfg__VERSION = '0.1.2';
 
?>
