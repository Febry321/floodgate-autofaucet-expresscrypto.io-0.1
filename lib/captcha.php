<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

/* Echoes the HTML code of the CAPTCHA for embedding it in a form. */
function embed_captcha($captchaType=1) {
 
  
     if($captchaType == 1){
      global $cfg_solvemedia_public;
      require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/solvemedia.php';
      echo solvemedia_get_html($cfg_solvemedia_public, null, true);	
    } 
    else if ($captchaType == 2) {
      global $cfg_raincaptcha_public;
     ECHO' <script src="https://raincaptcha.com/base.js" type="application/javascript"></script>
      <!-- an html form begin -->
      <div id="rain-captcha" data-key="' . $cfg_raincaptcha_public . '"></div>';
  }
  
  else if ($captchaType == 3) {
    global $cfg_recaptcha_site_key;
    echo '<script src="https://www.google.com/recaptcha/api.js" async defer></script>';
    echo '<div class="g-recaptcha" data-sitekey=' . $cfg_recaptcha_site_key . '></div>';
  }
  else if($captchaType == 4){
    global $cfg_h_captcha_site_key;
    echo '<script src="https://hcaptcha.com/1/api.js" async defer></script>';
    echo '<div class="h-captcha" data-sitekey=' . $cfg_h_captcha_site_key . '></div>';
  } 

  }
  /* Used in verify.php, return true if the CAPTCHA is successfully solved, or false. */
  function verify_captcha($captchaType=1) {
  
   //  H CAPTCHA
    //  SOLVE MEDIA
    if($captchaType==1){
      global $cfg_solvemedia_private;
      global $cfg_solvemedia_hash;
      require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/solvemedia.php';
      $privkey=$cfg_solvemedia_private;
      $hashkey=$cfg_solvemedia_hash;
      $solvemedia_response = solvemedia_check_answer($privkey,
                $_SERVER["REMOTE_ADDR"],
                $_POST["adcopy_challenge"],
                $_POST["adcopy_response"],
                $hashkey);
      if (!$solvemedia_response->is_valid) {
        //handle incorrect answer
        return false;
      }
      else {
        return true;
      }
  }



    //  RAIN CAPTCHA
    else if ($captchaType == 2) {
  
      global $cfg_raincaptcha_secret;
  
      $client = new \SoapClient('https://raincaptcha.com/captcha.wsdl');
      $response = $client->send($cfg_raincaptcha_secret, $_POST['rain-captcha-response'], $_SERVER['REMOTE_ADDR']);
      
      if ($response->status === 1) {
          // success
          return true;
      } else {
          // failed
          return false;
      }
      
    }  
    
      // google captcha
  else if($captchaType == 3){
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
      //your site secret key
      global $cfg_recaptcha_secret_key;
      //get verify response data
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$cfg_recaptcha_secret_key.'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);
      if($responseData->success){
        return true;
      } else{
        return false;
      }
  }
  }

  else if($captchaType==4){
  
    global $cfg_h_captcha_secret_key;
    if(!isset($_POST['h-captcha-response'])){
      die("Failed to verify Captcha!");
    }
    $post_data = [
      'secret' => $cfg_h_captcha_secret_key,
      'response' => $_POST['h-captcha-response']
    ];
    $post_context = stream_context_create([
      'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($post_data)
      ]
    ]);
    $url = 'https://hcaptcha.com/siteverify';
    $response = json_decode(file_get_contents($url, false, $post_context));
    return ($response && $response->success);
  } 
  
  }
?>
